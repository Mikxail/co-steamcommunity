var SteamCommunity = require('../index.js');

SteamCommunity.prototype.getMarketFilter = function(options, callback){
    options = options || {};

    var self = this;
    var appid = options.appid;

    if (!appid) return callback(new Error("appid parameter should be defined"));

    this.request({
        "uri": "https://steamcommunity.com/market/appfilters/" + appid,
        "json": true
    }, function(err, response, body){
        if(self._checkHttpError(err, response, callback)) {
            return;
        }

        if (body.success != 1) return callback(new Error("Error " + body.success));

        var filters = new CMarketFilters(appid, body);
        callback(null, filters);
    });
};

function CMarketFilters(appid, body) {
    this._appid = appid;
    var facets = body.facets;

    this.filters = {};

    for (var key in facets) {
        var filterName = (key + "").replace(new RegExp("^" + this._appid + "_"), "");
        this.filters[filterName] = facets[key];
    }
}

CMarketFilters.prototype.getFilter = function (filterName) {
    return this.filters[filterName] || {};
};
