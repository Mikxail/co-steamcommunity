'use strict';

var Util = require('util');
var Request = require('request');
var thenifyAll = require('thenify-all');
var CoRequest = require('co-request');
var _SteamCommunity = require('steamcommunity');

var __coRequest = CoRequest.defaults();


function SteamCommunity (options){
    options = options || {};
    _SteamCommunity.call(this, options);

    this.request = options.request || Request.defaults({});
    if (!isCoRequest(this.request)) {
        this.request = CoRequest.defaults(this.request);
    }
}

function isCoRequest(r) {
    if (r === CoRequest || r + "" === __coRequest + "") {
        return true;
    }
    return false;
}

Util.inherits(SteamCommunity, _SteamCommunity);

module.exports = SteamCommunity;

require('./classes/CMarketFilters');


thenifyAll.withCallback(SteamCommunity.prototype, SteamCommunity.prototype, [
    'getSteamUser',
    'getMarketFilter',
    'marketSearch',
    'getMarketItem'
]);
